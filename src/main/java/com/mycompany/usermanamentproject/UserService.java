/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanamentproject;

import java.util.ArrayList;

/**
 *
 * @author Lenovo
 */
public class UserService {
    private ArrayList<User> userlist;
    private int lastid =1 ;

    public UserService() {
        userlist = new ArrayList<User>();
    }
    public User addUser(User newUser){
       newUser.setId(lastid++);
       userlist.add(newUser);
       return newUser;
    }
    public User getUser(int index){
        return userlist.get(index);
    }
    public ArrayList<User> getUsers(){
        return userlist;
    }
    public int getSize(){
        return userlist.size();
    }
    
    
    public void logUserList(){
        for(User u:userlist){
            System.out.println(u);
        }
    }

    User updateUser(int Index, User updatedUser) {
        User user =  userlist.get(Index);
        user.setLogin(updatedUser.getLogin());
        user.setName(updatedUser.getName());
        user.setPassword(updatedUser.getPassword());
        user.setGender(updatedUser.getGender());
        user.setRole(updatedUser.getRole());
        return user;
    }

    User deleteUSer(int index) {
        return userlist.remove(index);
    }

}